import React from 'react';
import { StackNavigator } from 'react-navigation';

import COLORS from '../styles/colors.js';
import AppIcon from '../components/app_icon.js';
import DeleteButton from '../components/delete_button.js';
import HomeScreen from '../screens/home_screen.js';
import PadScreen from '../screens/pad_screen.js';

const RootStackNavigator = StackNavigator(
  {
    Home: {
      screen: HomeScreen,
      navigationOptions: {
        headerTitle: <AppIcon />,
        headerStyle: {
          backgroundColor: COLORS.statusBar
        },
        headerTintColor: COLORS.primaryText,
        title: `barfax`,
        titleStyle: {
          alignSelf: `center`
        }
      }
    },

    Pad: {
      screen: PadScreen,
      navigationOptions: {
        headerRight: <DeleteButton />,
        headerTitle: <AppIcon />,
        headerStyle: {
          backgroundColor: COLORS.statusBar
        },
        headerTintColor: COLORS.primaryText,
        title: `barfax`,
        titleStyle: {
          alignSelf: `center`
        }
      }
    }
  }
);

export default class RootNavigator extends React.Component {
  render() {
    return <RootStackNavigator />;
  }
}
