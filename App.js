import React from 'react';
import { Container } from 'native-base';
import { MaterialIcons } from '@expo/vector-icons';
import { Platform, StatusBar, View } from 'react-native';
import { AppLoading } from 'expo';

import cacheAssets from './utils/cacheAssets.js';
import RootNavigation from './navigation/RootNavigation.js';
import COLORS from './styles/colors.js';

export default class App extends React.Component {
  state = {
    preFetchDone: false
  };

  componentWillMount() {
    this._loadAssets();
  }

  async _loadAssets() {
    try {
      const fonts = [
          MaterialIcons.font,
          {
            'fira-mono': require(`./assets/fonts/FiraMono-Regular.ttf`),
            'lato-regular': require(`./assets/fonts/Lato-Regular.ttf`),
            'monoid-regular': require(`./assets/fonts/Monoid-Regular.ttf`),
            'open-dyslexic': require(`./assets/fonts/OpenDyslexicMono-Regular.ttf`),
            'Roboto': require(`native-base/Fonts/Roboto.ttf`),
            'Roboto_medium': require(`native-base/Fonts/Roboto_medium.ttf`)
          }
        ],

        imgs = [
          require(`./assets/icons/app-icon.png`),
          require(`./assets/images/pencil.png`)
        ];

      await cacheAssets(fonts, imgs);
    } catch(err) {
      console.warn(`Caching failed!`);
      console.log(err.message);
    } finally {
      this.setState({preFetchDone: true});
    }
  }

  render() {
    if (this.state.preFetchDone) {
      return (
        <Container style={{backgroundColor: COLORS.primary}}>
          {Platform.OS === `ios` && <StatusBar barStyle={`default`} />}
          {Platform.OS === `android` &&
            <View style={styles.statusBarUnderlay} />}
          <RootNavigation />
        </Container>
      );
    } else {
      return (
        <AppLoading />
      );
    }
  }
}

const styles = {
  statusBarUnderlay: {
    height: 24,
    backgroundColor: COLORS.statusBarUnderlay
  }
};
