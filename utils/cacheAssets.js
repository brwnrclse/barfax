/* Taken and modified from expo template code */
import { Asset, Font } from 'expo';

const cacheFonts = (fonts) => {
    return fonts.map((font) => {
      return Font.loadAsync(font);
    })
  },

  cacheImgs = (imgs) => {
    return imgs.map((img) => {
      return Asset.fromModule(img).downloadAsync();
    });
  };

export default function cacheAssets(fonts = [], imgs = []) {
  const fontAssets = cacheFonts(fonts),
    imgAssets = cacheImgs(imgs);

  return Promise.all([
    ...imgAssets,
    ...fontAssets
  ]);
}
