import DyslexicText from '../components/dyslex_text.js';
import FiraText from '../components/fira_text.js';
import LatoText from '../components/lato_text.js';
import MonoidText from '../components/monoid_text.js';

const getFontComponent = (font) => {
  switch(font) {
    case `Monoid`: return (MonoidText);
    case `Lato`: return (LatoText);
    case `Dyslexic`: return(DyslexicText);
    default: return(FiraText);
  }
}

export default getFontComponent;
