import React from 'react';
import { Container, Content } from 'native-base';

import COLORS from '../styles/colors.js';
import Editor from '../components/editor.js';

const PadScreen = () => {

  return (
    <Container style={styles.container}>
      <Content style={styles.content}>
        <Editor />
      </Content>
    </Container>
  );
};

const styles = {
  container: {
    flex: 1,
    justifyContent: `center`,
    backgroundColor: COLORS.barpad
  },

  content: {
    flex: 1
  }
}

export default PadScreen;
