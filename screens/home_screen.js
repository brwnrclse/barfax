import React from 'react';
import PropTypes from 'prop-types';
import { Container, Content } from 'native-base';

import COLORS from '../styles/colors.js';
import getFontComponent from '../utils/getFont.js';
import CreateButton from '../components/create_button.js';

const HomeScreen = ({navigation}) => {
  const StyledText = getFontComponent(`Fira`);

  const goToPad = () => {
    navigation.navigate(`Pad`);
  }

  return (
    <Container style={styles.container}>
      <Content>
        <StyledText style={styles.homeText}>
          {`This is barfax.`}
        </StyledText>
        <CreateButton clickHandler={goToPad}/>
      </Content>
    </Container>
  );
}

HomeScreen.propTypes = {
  navigation: PropTypes.object
}

const styles = {
  container: {
    flex: 1,
    justifyContent: `center`,
    backgroundColor: COLORS.barpad
  },

  homeText: {
    color: COLORS.primaryText
  }
};

export default HomeScreen;
