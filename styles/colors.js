const COLORS = {
  'primary': `#212121`,
  'primaryAlt': `#fafafa`,
  'primaryText': `#fafafa`,
  'primaryTextAlt': `#212121`,
  'statusBar': `#212121`,
  'statusBarAlt': `#333333`,
  'statusBarText': `rgba(250, 250, 250 0.8)`,
  'statusBarUnderlay': `rgba(0, 0, 0, 0.2)`,
  'barpad': `#333333`,
  'barpadAlt': `#fafafa`
};

export default COLORS;
