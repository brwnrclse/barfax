import React from 'react';
import { Text } from 'native-base';
import { Col, Row, Grid } from 'react-native-easy-grid';

import COLORS from '../styles/colors.js';

const Editor = () => {
  return (
    <Grid style={styles.grid}>
      <Col>
        <Row size={1} style={styles.pickList}>
          <Text>{`PickList`}</Text>
        </Row>
        <Row size={0.5} style={styles.title}>
          <Text>{`Title`}</Text>
        </Row>
        <Row size={7} style={styles.pad}>
          <Text>{`Pad`}</Text>
        </Row>
        <Row size={1} style={styles.stats}>
          <Text>{`Stats`}</Text>
        </Row>
      </Col>
    </Grid>
  );
}

const styles = {
  grid: {
    flex: 1,
    backgroundColor: COLORS.primaryAlt
  },

  pickList: {
    backgroundColor: `red`
  },

  title: {
    backgroundColor: `blue`
  },

  pad: {
    backgroundColor: `yellow`,
  },

  stats: {
    backgroundColor: `green`
  }
};

export default Editor;
