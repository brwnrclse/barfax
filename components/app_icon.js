import React from 'react';
import { Image } from 'react-native';

const TitleIcon = () => {
  return (
    <Image
      source={require(`../assets/icons/app-icon.png`)}
      fadeDuration={0}
      style={{
        alignSelf: `center`,
        width: 30,
        height: 30
      }}
    />
  );
}

export default TitleIcon;
