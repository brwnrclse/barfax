import React from 'react';
import { Button } from 'native-base';
import { MaterialIcons } from '@expo/vector-icons';

import COLORS from '../styles/colors.js';

const DeleteButton = () => {
  return (
    <Button style={{backgroundColor: COLORS.primary}}>
      <MaterialIcons name={`delete`} size={20} color={COLORS.primaryAlt}/>
    </Button>
  );
};

export default DeleteButton;
