import React from 'react';
import PropTypes from 'prop-types';
import { Text } from 'react-native';

const MonoidText = (props) => {
  return (
    <Text
      {...props}
      style={[props.style, {
        fontFamily: `monoid-regular`,
        fontSize: 15
      }]}
    />
  );
};

MonoidText.propTypes = {
  style: PropTypes.object
};

export default MonoidText;
