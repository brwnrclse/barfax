import React from 'react';
import PropTypes from 'prop-types';
import { Text } from 'react-native';

const LatoText = (props) => {
  return (
    <Text
      {...props}
      style={[props.style, {
        fontFamily: `lato-regular`,
        fontSize: 20
      }]}
    />
  );
};

LatoText.propTypes = {
  style: PropTypes.object
};

export default LatoText;
