import React from 'react';
import PropTypes from 'prop-types';
import { Button } from 'native-base';
import { Image } from 'react-native';

import COLORS from '../styles/colors.js';

const CreateButton = ({clickHandler}) => {
  return (
    <Button style={styles.button} onPress={clickHandler}>
      <Image
        source={require(`../assets/images/pencil.png`)}
        fadeDuration={0}
        style={{width: 25, height: 25}}
      />
    </Button>
  );
};

CreateButton.propTypes = {
  clickHandler: PropTypes.func
}

const styles = {
  button: {
    alignSelf: `center`,
    backgroundColor: COLORS.barpad,
    zIndex: 10000000
  }
};

export default CreateButton;
