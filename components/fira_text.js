import React from 'react';
import PropTypes from 'prop-types';
import { Text } from 'react-native';

const FiraText = (props) => {
  return (
    <Text
      {...props}
      style={[props.style, {
        fontFamily: `fira-mono`,
        fontSize: 15
      }]}
    />
  );
};

FiraText.propTypes = {
  style: PropTypes.object
};

export default FiraText;
