import React from 'react';
import PropTypes from 'prop-types';
import { Text } from 'react-native';

const DyslexicText = (props) => {
  return (
    <Text
      {...props}
      style={[props.style, {
        fontFamily: `fira-mono`,
        fontSize: 15
      }]}
    />
  );
};

DyslexicText.propTypes = {
  style: PropTypes.object
};

export default DyslexicText;
